## OpenSSH Server  
Simple SSH server with single user and base utilities.  
  
**Pros:**  
* based on clean Alpine linux  
* implemented fix of Alpine's absent root password - random password each launch  
* simple creation of user with couple of environment variables or from environment file  
* regular updates using latest official openssh-server packages  
  
## Settings  
You can configure container using simple environment variables, or right down then into file (secrets) and insert it into container.  
In order not to change host keys every time, you can mount `/etc/sshd` as volume. Host keys will be generated there by default.  
  
### Bootstrap  
Container's entrypoint creates user, starts sshd daemon and enters infinite loop or execute user's command from **CMD** environemnt variable. You can change that behaviour:  
1. Set **BOOTSTRAP** variable with shell code. Its code will be saved to file and executed after all basic settings like creation of users and running sshd.  
2. Create/mount scripts under _/bootstrap_. Entrypoint will look for _/bootstrap/\*.sh_ makes them executable and execute them in sorted order (using `sort -n`).  
3. Set **CMD** variable. In case of set CMD variable infinite loop will not be run. Entrpoint will run `eval "$CMD" "$*"`.  
  
### Configuration  
You can configure container it two ways: _via envrionment variables_ or _from config file_. In case if config file exists settings will be taken from there. In any case you can override some settings even having config file. Default path is _/config.yml_.  
Using of configuration file allows you to configure container more detailed.  
There are list of environment variables for configuration completely without config file.  
  
| Name | Required | Description | Example |  
| --- | --- | --- | --- |  
| USERNAME | yes | Name of user to create | j.smith |  
| PASSWORD | yes | **Hash** of password. | See generation notes below |  
| SSH_KEY | no | **Public** SSH key part for insertion to authorized_keys | See generation notes below |  
Using values above you can create single user (added to sudo group) and use it.  
  
Also you can set **SUDO_NOPASSWD** to 'yes' in case if you need to enable sudo without password. This environment variable will work even in case of using of configuration file. In case if you set both __sudo_nopasswd__ in YAML and **SUDO_NOPASSWD** in environment variables, sudo nopasswd mode will be enabled in case if at least one of settings are 'yes' (OR scheme).  
  
## Generation notes  
  
### SSH keys  
You can use either RSA or ED25519 (*preffered*) format. [More information](https://gist.github.com/JamesOBenson/a02f1c688d3d112a3694574a0a9f0adc).  
In order to generate RSA key use command:  
```bash  
ssh-keygen -t rsa -b 4096 -f /path/to/result/file  
```  
In order to generate ED25519 key use command:  
```bash  
ssh-keygen -t rsa -b ed25519  -o -a 150 -f /path/to/result/file  
```  
  
### User password hash  
You can generate hash [online](https://www.mkpasswd.net/?type=crypt-sha512), or use `mkpasswd` for this operation.  
In order to generate password hash use command:  
```bash  
mkpasswd -m sha-512 'my secret password'  
```  
You can omit specifing of password in arguments and it will be prompted interactive.  
  
## YAML configuration file  
In order to create more than one user, or add more than one public key, you can use configuration file. Use variable **CONFIG_PATH** to override path to configuration file inside the container.  
Example of configuration:  
```yaml  
# enable sudo nopasswd mode (can be ommited - considered as no)  
sudo_nopasswd: yes  
# users description  
users:  
  tunnel: # user for making tunnels, no authorized_keys are needed  
    password: '$1$EhF.43ER$Z41ozL789VYi.N36rD7wL1'  
    shell: /bin/false  
  john: # john do need password just authorized_key  
    authorized_keys: |  
      ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGP3Proqvg4BTF5XqKtk17zr+sy7EYM8Lq+b18TgBSTX john@host  
    # can obtain superuser permissions via sudo  
    sudo: yes  
  someone:  
    # password can be provided in both md5 and sha512 formats  
    password: '$6$aOj5JUFSf$N3c2KGrv1A1m/RkYko7LimMSWiIXuY/6g18kg0PEz8HtnrTzP2CHkwdS.xu5Kcf3JiIagHljVuIHoiGWalUJa/'  
    # lets add several keys  
    authorized_keys: |  
      ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGP3Proqvg4BTF5XqKtk17zr+sy7EYM8Lq+b18TgBSTX first@one  
      ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGP3Proqvg4BTF5XqKtk17zr+sy7EYM8Lq+b18TgBSTX second@one  
    # omiting sudo is considered as no  
```  