
alias rm="rm -i"
alias mv="mv -i"
alias cp="cp -i"
alias ll="ls -lh --color=auto"
alias l="ls -lh --color=auto"
alias la="ls -lhA --color=auto"
alias a="ls -lhA --color=auto"
alias lsl="ls -lh --color=auto"
alias lsa="ls -lhA"
alias ..="cd .. && ls -lhA --color=auto"
alias e="exit"
alias s="sudo"
alias S="sudo -i"
alias svim="sudo vim"
alias svi="sudo vim"
alias p='ping 8.8.8.8'

NORMAL="\[\e[0m\]"
RED="\[\e[1;31m\]"
WHITE="\[\e[1;37m\]"
BLUE="\[\e[1;34m\]"

if [ "$USER" = root ]; then
	export PS1='$WHITE[ $RED\u$WHITE@\h: $RED\w$WHITE ] # $NORMAL'
else
	export PS1='$WHITE[ $BLUE\u$WHITE@\h: $RED\w$WHITE ] \$ $NORMAL'
fi