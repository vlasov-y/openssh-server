#!/usr/bin/env sh

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
trap "exit" INT
set -eo pipefail

init() {
	syslogd 		# starting log daemon
	if ! ls /etc/sshd/ssh_host_* 2>/dev/null 1>&2; then
		ssh-keygen -A 	# generating new server SSH keys
		mkdir -p /etc/sshd
		mv -f /etc/ssh/ssh_host_* /etc/sshd
	else
		echo 'ssh-keygen: found host keys: '
		for i in /etc/sshd/*.pub; do
			echo "  `ssh-keygen -lE MD5 -f "$i"`"
		done
	fi
	`command -v sshd`	# starting sshd daemon
}

reset_passwords() {
	# set random passwords to everyone
	for U in `getent passwd | cut -d: -f1`; do
		P="`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 32`" || true 
		printf "${U}:${P}" | chpasswd 2>/dev/null
	done
}

create_user() {
	U="$1"	# user
	P="$2"	# password
	S="${3:-/bin/sh}"	# shell
	SUDO="$4"
	# create users
	if ! id -u "$U" 1>/dev/null 2>&1; then
		adduser -D -s "$S" "$U"
		echo "user: created $U"
	fi
	if [ "$(getent passwd "$U" | cut -d: -f7)" != "$S" ]; then
		usermod -s "$S" "$U" 1>/dev/null
		echo "user: set $S for $U"
	fi
	if echo "$SUDO" | grep -qiE '1|yes|true' && ! groups "$U" | grep -q 'sudo'; then
		addgroup "$U" sudo
		echo "user: added ${U} to sudo group"
	fi
	if [ "$P" != '' ]; then
		echo "${U}:${P}" | chpasswd -e 2>/dev/null
	fi
}

append_authorized_key() {
	U="$1"
	KEY="$2"
	SSH_PATH="`getent passwd "$U" | cut -d: -f6`/.ssh"
	mkdir -p "$SSH_PATH"
	if [ ! -f "${SSH_PATH}/authorized_keys" ] || ! grep -qF "$KEY" "${SSH_PATH}/authorized_keys"; then
		echo "$KEY" >> "${SSH_PATH}/authorized_keys"
	fi
	chown -R "`id -u ${U}`:`id -g ${U}`" "$SSH_PATH"
	chmod 0700 "$SSH_PATH"
	chmod 0600 "${SSH_PATH}/authorized_keys"
}

enable_sudo_nopasswd() {
	echo 'sudo: enabled NOPASSWD for all users in sudo group'
	echo '%sudo ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/sudo
}

main() {
	if [ "$USERNAME" != '' ] && [ "$PASSWORD" != '' ]; then
		create_user "$USERNAME" "$PASSWORD" "/bin/sh" "yes"	# user with sudo permissions 
		append_authorized_key "$USERNAME" "$SSH_KEY"
	elif [ -f "$CONFIG_PATH" ]; then
		# parse config file
		echo "config: reading ${CONFIG_PATH}"
		for U in `yq r "$CONFIG_PATH" users | grep -oE '^[^ :]+'`; do
			P="`yq r "$CONFIG_PATH" "users.\"${U}\".password"`"	# password
			KEYS="`yq r "$CONFIG_PATH" "users.\"${U}\".authorized_keys" | grep -v null || true`"
			SHELL="`yq r "$CONFIG_PATH" "users.\"${U}\".shell" | grep -v null || true`"
			SUDO="`yq r "$CONFIG_PATH" "users.\"${U}\".sudo" | grep -v null || true`"
			create_user "$U" "$P" "$SHELL" "$SUDO"
			append_authorized_key "$U" "$KEYS"
		done
		# check nopasswd for sudo from config file
		if yq r "$CONFIG_PATH" sudo_nopasswd | grep -Fq true; then
			enable_sudo_nopasswd
		fi
	else
		echo 'error: you must specify both USERNAME and PASSWORD or pass CONFIG_PATH' 1>&2
		exit 1
	fi
	# check nopasswd for sudo from environemnt variable
	if echo "$SUDO_NOPASSWD" | grep -qiE '1|true|yes'; then
		enable_sudo_nopasswd
	fi
	# info message
	echo "sshd: server started on `netstat -tunlp | awk '/sshd/{print $4}' | head -1`"
}

init
reset_passwords
main

# execute bootstrap shell code from env variables
if [ "$BOOTSTRAP" != '' ]; then
	T="`mktemp`"
	cat <<EOF >"$T"
$BOOTSTRAP
EOF
	/bin/sh -ex "$T"
	rm -f "$T"
fi

# execute all found bootstrap scripts
for F in `find /bootstrap -type f -name '*.sh' 2>/dev/null | sort -n`; do
	/bin/sh -ex "$F"
done

# infinite loop or entrypoint command
if [ "$CMD" != '' ]; then
	echo "cmd: $CMD $*"
	eval "$CMD" "$*"
else
	echo "cmd: infinite loop"
	while true; do
		tail -f /var/log/messages | grep sshd | cut -d ' ' -f 6-
	done
fi

