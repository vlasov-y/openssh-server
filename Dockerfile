ARG ALPINE_VERSION=latest
FROM alpine:${ALPINE_VERSION}
ENV CONFIG_PATH /config.yml
ENTRYPOINT [ "/sbin/tini", "--", "/entrypoint.sh" ]

# adding user, installing sudo
RUN apk upgrade --update --no-cache && \
	apk add --update --no-cache sudo ca-certificates openssh rsync tini shadow && \
	addgroup sudo && sed '/^%.+/d' && echo '%sudo ALL=(ALL) ALL' > /etc/sudoers.d/sudo

# download and install and yq
ADD https://github.com/mikefarah/yq/releases/download/2.4.0/yq_linux_amd64 /bin/yq

# copying configs and scripts
COPY ./customization/sshd_config /etc/ssh/sshd_config
COPY ./customization/motd /etc/motd
COPY ./customization/profile.sh /etc/profile.d/
COPY ./entrypoint.sh /entrypoint.sh

# fixing permissions
RUN chown root:root /entrypoint.sh /bin/yq && \
	chmod 0755 /entrypoint.sh /bin/yq && \
	chmod a+rwxt /tmp
